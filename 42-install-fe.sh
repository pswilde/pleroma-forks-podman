#!/bin/bash

REF=stable
if [[ $1 != "" ]]; then
	REF=$1
fi
source ./script_config.sh

echo "Install pleroma-fe? (y/N)"
read -r pfe
if [[ $pfe == "y" ]]; then
    echo "Installing pleroma-fe..."
    podman exec -it $POD_NAME-web $POD_OPT/bin/pleroma_ctl frontend install pleroma-fe --ref=$REF
    echo "Done."
fi
echo "Install admin-fe? (y/N)"
read -r afe
if [[ $afe == "y" ]]; then
    echo "Installing admin-fe..."
    podman exec -it $POD_NAME-web $POD_OPT/bin/pleroma_ctl frontend install admin-fe  --ref=$REF
    echo "Done."
fi
