#!/bin/bash

curl -L https://gitlab.com/soapbox-pub/soapbox/-/jobs/artifacts/develop/download?job=build-production -o soapbox.zip
unzip soapbox.zip -d ./static
mv ./static/static/* ./static/
mv ./static/static/.* ./static/
rm ./static/static -r
