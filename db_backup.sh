#!/bin/bash

mydir=$(dirname "${BASH_SOURCE[0]}")

source $mydir/script_config.sh

mkdir $mydir/db_backups -p
TS=$(date +%Y%m%d%H%M%S)
backupname=db-$TS-backup.sql
compress="-Fc"
for arg in $@
do
	case $arg in
		"nocompress")
			echo "Not using Compression"
			compress=""
			;;
		*)
			echo "Setting Backup Name : $arg"
			backupname=$arg
			;;
	esac
done
backupfile=$mydir/db_backups/$backupname
echo "Backing up database to : $backupfile"
podman exec $POD_NAME-db pg_dump -U $PG_USER $PG_NAME $compress > $backupfile
echo Done.
