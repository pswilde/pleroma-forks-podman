#!/bin/bash

mkdir ./uploads ./static ./config -p

chmod -R 777 uploads static

source ./script_config.sh

podman pod create \
    --name $POD_NAME-pod \
    -p $PORT:4000 $EXTRAPARMS

