#!/bin/bash

source ./script_config.sh
cp ./pod.service.example ./$POD_NAME.service

POD_STR=s/++POD_NAME++/$POD_NAME/
sed -i $POD_STR ./$POD_NAME.service
USER=$(whoami)
USR_STR=s/++USER++/$USER/
sed -i $USR_STR ./$POD_NAME.service

echo "Copying $POD_NAME.service to /etc/systemd/system..."
sudo cp ./$POD_NAME.service /etc/systemd/system/
echo "Enabling service... (you can start it with 'sudo systemctl start $POD_NAME.service')"
sudo systemctl enable $POD_NAME
echo "Done."

