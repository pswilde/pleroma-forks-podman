#!/bin/bash

source ./script_config.sh

mkdir ./postgres ./db_backups -p

podman run -d \
    --name $POD_NAME-db \
    --pod $POD_NAME-pod \
    -e POSTGRES_USER=$PG_USER \
    -e POSTGRES_PASSWORD=$PG_PASS \
    -e POSTGRES_DB=$PG_NAME \
    -v ./postgres:/var/lib/postgresql/data \
    -v ./db_backups:/backups \
    docker.io/postgres:15.2-alpine

