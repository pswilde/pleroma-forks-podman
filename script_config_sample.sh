#!/bin/bash

DATE=$(date +"%Y-%m-%d")
PORT=4000
POD_NAME=akkoma
INSTANCE_SW=akkoma # or pleroma or rebased
PG_USER=akkoma
PG_PASS=akkoma
PG_HOST=localhost
PG_NAME=akkoma
INSTANCE_VER=develop
EXTRAPARMS=""

INSTANCE_IMG="$INSTANCE_SW-$INSTANCE_VER-$DATE"
if [ -f ./.last ]; then
	INSTANCE_IMG=$(cat ./.last)
fi
POD_OPT="/opt/pleroma"
POD_LIB="/var/lib/pleroma"
if [[ $INSTANCE_SW == "akkoma" ]]; then
	POD_OPT="/opt/akkoma"
	POD_LIB="/var/lib/akkoma"
fi
	
echo $INSTANCE_IMG
