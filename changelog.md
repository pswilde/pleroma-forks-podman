# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Fixed

### Changed


## 2023-03-20

### Added

### Fixed

### Changed

- Updated Elixir Build image to 1.14.3
- Updated Alpine run images to 3.17.2
- **Breaking** : Upgraded DB to Postgres 15, backup and restore your database

### Removed
