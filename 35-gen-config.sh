#!/bin/bash

source ./script_config.sh

podman exec -it --user=0 --privileged $POD_NAME-web $POD_OPT/bin/pleroma_ctl instance gen
podman exec $POD_NAME-web cat config/generated_config.exs > ./config/prod.secret.exs
echo .
echo ===============================
echo "Check files in config/prod.secret.exs - if all looks good, restart your container"
