#!/bin/bash

source ./script_config.sh

podman pod stop $POD_NAME-pod
podman pod rm $POD_NAME-pod
./10-create-pod.sh
./20-create-db.sh
./30-run.sh
