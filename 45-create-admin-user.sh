#!/bin/bash

source ./script_config.sh
admin=""
echo "Username:"
read -r user
echo "Email:"
read -r email
echo "Make Admin? (y/N)"
read -r makeadmin
if [[ $makeadmin == "y" ]]; then
    admin="--admin"
fi
podman exec -it $POD_NAME-web $POD_OPT/bin/pleroma_ctl user new $user $email $admin
echo Done.
