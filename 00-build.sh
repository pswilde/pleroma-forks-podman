#!/bin/bash

if [ ! -f ./script_config.sh ]; then
	cp ./script_config_sample.sh ./script_config.sh
	echo "Script config file created. Please edit it to your liking and then re-run this script"
	exit
fi

source ./script_config.sh

# remove cached old INSTANCE_IMG ref
touch ./.last
rm ./.last

podman build -f Containerfile-$INSTANCE_SW --build-arg="BUILD_DATE=$DATE" --build-arg="VCS_REF=$INSTANCE_VER" -t $INSTANCE_IMG

ex=$?
if [[ $ex != 0 ]]; then
	echo $ex
	echo "Failed to compile"
	exit
fi
if [[ $ex == 0 ]]; then
	echo "Creating .last file..."
	if [ -f ./.last ]; then
		rm ./.last
	fi
	echo $INSTANCE_IMG > ./.last
fi

