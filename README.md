# Pleroma, Akkoma and Rebased

[Pleroma](https://pleroma.social") is a federated social networking
platform, compatible with Mastodon, MissKey, GNU Social, and other ActivityPub implementations. It is free software licensed uner the AGPLv3

Its main advantages are its lightness and speed.

[Akkoma](https://akkoma.social) and [Rebased](https://soapbox.pub) are forks of Pleroma adding additional and different features.

## Features

These podman scripts will compile a container image of your chosen instance software and allow you to run it on your own server. A little bit of knowledge of reverse proxies will be helpful, but the [Akkoma git repo](https://akkoma.dev/AkkomaGang/akkoma) has a useful nginx config here: https://akkoma.dev/AkkomaGang/akkoma/src/branch/develop/installation/nginx  
The scripts are numbered so to help run them in the desired order (see below).  

## Build-time variables

Variables are entered into `script_config.sh` - (copy `script_config_sample.sh`), 
the key variables being `PG_USER`, `PG_PASS`, `PG_HOST`, `PG_NAME` if you have a database running elsewhere

### Installation

Each script is numbered which give the order in which they need to be run - scripts with `5` in the name should only need to be run once.  
 1. `00-build.sh` - this will build the latest commit from the software's git repo. Re-running this scripts will effectively update the version of the software. It may take 10 minutes or more to compile.
 2. `10-create-pod.sh` - this creates a simple pod, in which the other containers will exist. It will also open the port seting in your config for incoming web access and map the folders in which persistent data will reside.
 3. `20-create-db.sh` - this will create a postgres container in which the database will exist
 4. `30-run.sh` - this creates the main container and starts the web server (you'll need a reverse proxy (HAProxy, httpd, nginx, etc.) to publish it). At start up, this will check the database and create the appropriate tables and extensions so things will work.
 5. `35-gen-config.sh` - YOU SHOULD ONLY NEED TO RUN THIS ONCE. Will generate a `generated_config.exs` and export it to `config/prod.secret.exs` file. Check your config in in and it ok, run `30-run.sh` again to apply the updated config. TAKE NOTE; The port set in this config is not the same as the port set in your script-config.sh file, probably best to leave this one as it is.
 6. Now, you're instance should be running - test it and find out.
 7. `42-install-fe.sh` - this installs the pleroma-fe and admin-fe front ends - which is where you will log in and ultimately use the system. You probably only need to use this for Akkoma and/or Pleroma, but it will work for Rebased too.
 8. `43-REBASED-install-soapbox.sh` - will install the soapbox frontend. This is only really necessary for Rebased, but it will work for Akkoma and Pleroma too.
 9. `45-create-admin-user.sh` - creates your first user, as admin and provides a password reset link where you can set the password.
 10. Now, you should be done. Well done!

Once the above steps are complete, you're nearly ready to go! The final steps are to 
 1. Configure your reverse proxy

#### Configure your reverse proxy
This will differ from device to device - but check some of the configs in the [Akkoma Gitea Repo](https://akkoma.dev/AkkomaGang/akkoma) for further information.  

### Congratulations!
If all has gone well, you should be done. Well done. Now get out there and talk to some people!


### Switching between Pleroma, Akkoma and Rebased
As these scripts work for all Pleroma, Akkoma and Rebased, it is indeed possible to switch the `INSTANCE_SW` variable in `script_config.sh` to another, rebuild and re-run the web container and you'll you'll then be running the new software.  
**MAKE SURE YOU BACK UP YOUR DATABASE IF YOU TRY THIS!**
However, here are a few of my notes when doing this myself: 
* At the date of writing (2023-02-02), switching between Pleroma and Rebased has been fine, I have switched between both of them a number of times with no problems whatsoever (just remember to install soapbox or at the very least run the install-fe script).
* Switching between Pleroma and Akkoma again seems functionally fine, but I have experienced numerous issues with database performance/timeouts and app crashes.
    * The app crashes seemed to have been due to using features not in Akkoma (i.e. Prometheus), but this seems to have been fixed in later versions of Akkoma.
    * The database performance is still something I have not got around - maybe it's due me having a large database, but still things work fine in Pleroma with the same size database so currently I am clueless on this. Running a clean Akkoma database seems to work fine, so be cautious when changing from Pleroma to Akkoma if you have a larger database.
    * I've also experienced spurious federation issues, i.e. not being able to federate with some instances. This is weird as switching back to Pleroma everything is fine again.

## Final Notes

As with anything, I've only tested this on my systems, I have tried my best to re-run these scripts to ensure all events are covered, but I may have missed some.  
If you do have troubles, either create an issue here, or contact me on the Fediverse at
[@paul@notnull.click](https://notnull.click/@paul) and I'll try to help.

